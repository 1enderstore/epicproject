freq = 2;
decay = 4;
delay = .05;
dur = .12;

myDelay = (textIndex-1)*delay;
t = time - (inPoint + myDelay);
startVal = 100;
endVal = 0;

if(t < dur){
  linear(t,0,dur,startVal,endVal);
}else{
  amp = (endVal - startVal)/dur;
  w = freq*Math.PI*2;
  endVal + amp*(Math.sin(t*w)/Math.exp(decay*t)/w);
}