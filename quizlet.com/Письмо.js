// Письмо
(function () {
    function qwrite() {
        if (document.querySelector('.WriteProgress-value').innerHTML > 0 && qfuncIsStarted == false) {
            qfuncIsStarted = true;
            console.log('%cSUCCESS EVENT STARTED!', 'background: #222; color: #bada55');
            document.querySelector('.AutoExpandTextarea-textarea').oninput = function () {
                setTimeout(() => {
                    document.querySelector('.TypeTheAnswerField-actions > .UIButton > .UIButton-wrapper').click();
                    document.querySelector('.WrittenFeedbackItem-answerOverride > .UILink').click();
                    qfuncIsStarted = false;
                }, 10);
            }
            setTimeout(() => {
                qwrite();
            }, 50);
        } else {
            qTryEventRegister();
        }
    }
    
    function qTryEventRegister() {
        console.log('%cTRYING TO EVENT REGISTER!', 'background: #222; color: #ffda55');
        setTimeout(() => {
            qwrite();
        }, 500);
    }

    qfuncIsStarted = false;
    qwrite();
})();
