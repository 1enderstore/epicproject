// Правописание
(function () {
    qwords = JSON.parse(prompt('Вставь сюда JSON данные из скрипта "Карточки":'));

    function qwrite() {
        if (document.querySelector('.SpellQuestionView-inputPrompt > .SpellQuestionView-inputPrompt--plain') &&
            document.querySelector('.AutoExpandTextarea-textarea').textContent.length <= 1) {
            console.log('%cSUCCESS EVENT STARTED!', 'background: #222; color: #bada55');
            qword = '';
            qwordexpl = document.querySelector('.SpellQuestionView-inputPrompt > .SpellQuestionView-inputPrompt--plain').innerHTML;

            qwords.forEach(function (item, i, arr) {
                if (item['0'] == qwordexpl) {
                    qword = item['1'];
                } else if (item['1'] == qwordexpl) {
                    qword = item['0'];
                }
            });

            if (qword) {
                qword += '♥';

                document.querySelector('.AutoExpandTextarea-textarea').value = qword;
                document.querySelector('.AutoExpandTextarea-textarea').textContent = qword;

                qwrite();
            }
        } else {
            console.log('%cTRYING TO EVENT REGISTER!', 'background: #222; color: #ffda55');
            setTimeout(() => {
                try {
                    qwrite();
                } catch (error) {

                }
            }, 500);
        }
    }
    qwrite();
})();