#!/bin/bash

clear

FILE_HOSTS=/etc/hosts

function check {
  if grep -q $HOST $FILE_HOSTS; then
    echo -e "[ \e[1;32mОК\e[0m ] \e[1;33m$HOST\e[0m > \e[1;32mУстановлен\e[0m"
  else
    echo -e "\e[1;33m$HOST\e[0m > \e[1;31mНе установлен\e[0m"
  fi
}

function install {
  if grep -q $HOST $FILE_HOSTS; then
    echo -e "[ \e[1;32mОК\e[0m ] \e[1;33m$HOST\e[0m > \e[1;32mУстановлен\e[0m"
  else
    echo -e "\e[1;33m$HOST\e[0m > \e[1;31mНе установлен\e[0m"

    if [[ $br = 0 ]]; then
      br=1
      echo "" >> $FILE_HOSTS
    fi

    echo "127.0.0.1 $HOST" >> $FILE_HOSTS
    if grep -q $HOST $FILE_HOSTS; then
      echo "Ок"
    else
      echo -e "[ \e[1;31mОшибка\e[0m ] Не удалось установить хост \e[1;33m$HOST\e[0m"
    fi
  fi 
}

HOST="license.sublimehq.com"
install

HOST="na1r.services.adobe.com"
install

HOST="hlrcv.stage.adobe.com"
install

HOST="lmlicenses.wip4.adobe.com"
install

HOST="lm.licenses.adobe.com"
install

HOST="activate.adobe.com"
install

HOST="practivate.adobe.com"
install