#!/bin/bash
clear
cd /tmp
wget https://download.sublimetext.com/sublime_text_3_build_3176_x64.tar.bz2
tar xvjf sublime*.tar.bz2
rm sublime*.tar.bz2

sudo mv /tmp/sublime* /opt/

clear
echo ""
echo "Icon=/opt/sublime_text_3/Icon/48x48/sublime-text.png"
echo "Exec=/opt/sublime_text_3/sublime_text %F"
echo ""
read -p "Нажмите любую кнопку для продолжения"

sudo nano /opt/sublime_text_3/sublime*.desktop
sudo rm /usr/share/applications/sublime*.desktop
sudo ln -s /opt/sublime*/sublime_text.desktop /usr/share/applications/
